import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def searchForFile(exact, label, size, type, offset, sort_field, sort_order, accesstoken, username, home_path, community_path):
    #request_data = '{"query": {"all":[{"type": "label","args": {"exact": true, "label": "ML_Project"}},{"type": "owner","args": {"owner": "pawanbole"}}]},"size": 10,"from": 0,"sort": [{"field": "dateCreated","order": "descending"}]}'

    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/search'
    if not exact:
        exact = False
    sort_field = "dateCreated"
    sort_order = "descending"

    request_Data = RequestCreator.create_searchFile_request(exact, label,  username, size, type, offset, sort_field, sort_order, home_path, community_path)

    r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Connection': 'keep-alive',
                                        'Content-Type': 'application/json'},
                                        data= request_Data)

    json_data = r.json()
    response = ResponseParser.parse_searchResponse(json_data, type,  home_path, community_path, username)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res
