class FileIDInfo:
    def __init__(self):
        self.infoType = ""
        self.path = ""
        self.sharecount  = 0
        self.datecreated = ""
        self.md5 = ""
        self.permission = ""
        self.datemodified = ""
        self.type = ""
        self.filesize = 0
        self.label = ""
        self.id = ""
        self.contenttype = ""
        self.dircount = 0
        self.filecount = 0
        self.ids = []
        self.hasSubDirs = False