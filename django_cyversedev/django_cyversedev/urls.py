"""Cyverse_Django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('base/', views.base, name='base'),
    path('middlepanel/', views.middlepanel, name='middlepanel'),
    path('admin/', admin.site.urls),
    path('', views.loadlogin, name='loadlogin'),
    path('login/', views.login, name='login'),
    path('loginCyverse/', views.loginCyverse, name='loginCyverse'),
    path('getSessionId/',views.getSessionId,name='getSessionId'),
    path('home/', views.home, name='home'),
    path('oauth/', views.oauth, name='oauth'),
    path('getFileIdApi/', views.getFileIdApi, name='getFileIdApi'),
    path('retrieveMetaData/', views.retrieveMetaData, name='retrieveMetaData'),
    path('saveMetaData/', views.saveMetaData, name='saveMetaData'),
    path('renderSharedData/', views.renderSharedData, name='renderSharedData'),
    path('getCommunityData/', views.getCommunityDataList, name='getCommunityData'),
    path('getRootPathUser/', views.getRootPathUser, name='getRootPathUser'),
    path('searchFile/', views.searchFile, name='searchFile'),
    path('searchUser/', views.searchtheUser, name='searchtheUser'),
    path('setPublicLink/', views.setPublicLink, name='setPublicLink'),
    path('remPublicLink/', views.remPublicLink, name='remPublicLink'),
    path('checkFilePermission/', views.checkFilePermission, name='checkFilePermission'),
    path('downloadFile/', views.downloadFile, name='downloadFile'),
    path('getSpeciesVersionList/', views.getSpeciesVersion, name='getSpeciesVersionList'),
    path('Signout/', views.Signout, name='Signout'),
    path('getAnalyze/', views.getAnalyze, name='getAnalyze')
]

