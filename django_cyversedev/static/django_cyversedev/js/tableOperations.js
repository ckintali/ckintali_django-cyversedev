function setupPagination(data, enable_prev, enable_next, defaultaction){
        var pages = 0
        var total = parseInt(data["total"])
        var pages_rem = total%recordsLimit
        if (pages_rem == 0)
        {
            pages = parseInt((total/recordsLimit))
        }
        else {
            pages = parseInt((total/recordsLimit)) + 1
        }
        if (pages == 0){
            $('#pagetext').hide();
            $('#page-selection').hide();
            return
        }
        else{
            $('#pagetext').show();
            $('#page-selection').show();
        }


        // logic to enable disable the next and previous
        if (defaultaction) {
            if (pages == 1) {
                enable_prev = false;
                enable_next = false;
            } else if (pages > 1) {
                 enable_next = '»';
                 enable_prev = false;
            }
        }
        else{
            var currentpage = parseInt($("#page-selection").data("currentPage"));
            if (currentpage == 1){
                enable_prev = false;
                enable_next = '»';
                $('#page-selection').bootpag({next: enable_next, prev: enable_prev})
            }
            else if(currentpage == pages){
                   enable_next = false;
                   enable_prev = '«';
                   $('#page-selection').bootpag({next: enable_next, prev: enable_prev})

            }
            else{
                enable_next = '»';
                enable_prev = '«';
                $('#page-selection').bootpag({next: enable_next, prev: enable_prev})
            }
        }

        // continue from here , to seperate the code.


        if (defaultaction){
        // init bootpag
        $('#page-selection').bootpag({
            total: pages,
            maxVisible: 5,
            next: enable_next,
            prev: enable_prev
        });
        }
            $('ul.pagination.bootpag li').click(function() {
                var num = $(this).attr('data-lp');
                $("#page-selection").data("currentPage", num)
             var section = $("#page-selection").data("currentSection")
             if (section == "home"){
                 getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, 'NAME', 'ASC','home', false)
             }
             else if (section == "shared"){
                 getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, 'NAME', 'ASC','shared', false)
             }
             else if(section == "community"){
                 getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, 'NAME', 'ASC','community', false)
             }
    });
}

function interpretData(data, functiontype){
    $('#fileFolder').children('tbody').html("")
    populateTable(data["folders"], "folders")
    populateTable(data["files"], "files")
    $("tr").on({contextmenu:function(e) {
            $("tr").removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass("file").toString() == "true") {
                e.preventDefault();
                $("#context-menu").data("fileInfo",$(this).data("info"))
                $('#context-menu').css({'display': 'block', 'left': e.pageX, 'top': e.pageY});
                $('#popup-menu').css({'display':'none'});
                rightPanelUI($(this).data("info"))

            }
        },click:function(e){
            $("tr").removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass("file").toString() == "true") {
                $(".right").data("fileInfo", $(this).data("info"))
            }
        },dblclick:function(){
            $("tr").removeClass('active');
            $(this).addClass('active');
            var fldrpth=""
            rightPanelUI($(this).data("info"))
            if($(this).hasClass("folder").toString()=="true") {
                  $("#searchRender").data("enableSearchRender", false)
                if (functiontype == 'getFileList') {
                    fldrpth =$(this).data("info").path;
                    getFileList(fldrpth, recordsLimit, 0, "NAME", "ASC","home", true)
                } else if (functiontype == 'getShareFileList') {
                    fldrpth = $(this).data("info").path;
                    getFileList(fldrpth, recordsLimit, 0, "NAME", "ASC","shared", true)
                } else if (functiontype == 'getCommunityData') {
                    fldrpth = $(this).data("info").path
                    getFileList(fldrpth, recordsLimit, 0, "NAME", "ASC","community", true)
               }
            }
        }
    });
    $(".popMenu").on({click:function(e) {
        $("tr").removeClass('active');
        var prntRow=$(this).closest("tr");
        prntRow.addClass('active');
        $('#popup-menu').css({'display':'block' ,'left': e.pageX, 'top': e.pageY});
        rightPanelUI(prntRow.data("info"))
       }
    });
    $('body').click(function(evt){
       if($(evt.target).closest("button").attr("class") != "btn popMenu"){
            $('#popup-menu').css({'display':'none'});
       }
       if(evt.target.id != "#context-menu"){
            $('#context-menu').css({'display':'none'});
       }
    });
}
function buildBreadCrumbs(data,functiontype) {
    $(".breadcrumb").html('')
    $(".breadcrumb").data("currentPath",data.fileSystemInfo.path)
    var link = ""
    if (functiontype == 'getFileList') {
        var directories = data.fileSystemInfo.path.split(homePath)[1]
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="disableSearchRender(); getFileList(homePath,recordsLimit,0,\'NAME\',\'ASC\',\'home\', true)"><i class="fas fa-home mr-1"></i></a></li>')
        if(directories!=null && directories!=''){
           link=""
           for (var dir in directories.split('/')) {
               if(directories.split('/')[dir] == '')
                   continue

                   link = link+"/"+ directories.split('/')[dir]
                   $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="disableSearchRender(); getFileList(\'' + homePath+link + '\',recordsLimit,0,\'NAME\',\'ASC\',\'home\', true)">' + directories.split('/')[dir] + '</a></li>')
           }
        }
    }
    else if (functiontype == 'getShareFileList') {
        var directories = data.fileSystemInfo.path.split(homePathforShare)[1]
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="disableSearchRender(); getFileList(homePathforShare,\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'shared\', true)"><i class="fas fa-user-friends mr-1"></i></a></li>')
        if(directories!=null && directories!=''){
            link=""
            for (var dir in directories.split('/')) {
                if(directories.split('/')[dir] == '')
                    continue

                link = link+"/"+ directories.split('/')[dir]
                $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="disableSearchRender(); getFileList(\'' + homePathforShare+link + '\',\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'shared\', true)">' + directories.split('/')[dir]+ '</a></li>')
            }
        }
    } else if (functiontype == 'getCommunityData') {
        var directories = data.fileSystemInfo.path.split(homePathforCommunity)[1]
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="disableSearchRender(); getFileList(homePathforCommunity,\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'community\', true)"><i class="fas fa-users mr-1"></i></a></li>')
        if(directories!=null && directories!=''){
            link=""
            for (var dir in directories.split('/')) {
                if(directories.split('/')[dir] == '')
                    continue
                link = link+"/"+ directories.split('/')[dir]
                $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="disableSearchRender(); getFileList(\'' +homePathforCommunity+link + '\',\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'community\', true)">' + directories.split('/')[dir]+ '</a></li>')
            }
        }
    }
}
