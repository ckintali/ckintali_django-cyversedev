var specVer;
function rightPanelUI(data){

    $(".right").data("fileInfo",data)
    $(".right").find('.pathLink').html("FilePath: "+data.path)
}
function rightPanel(section) {
    $(".middle").removeClass("col-10")
    $(".middle").addClass("col-7")
    $(".right").css("display", "block")
    rpStatus = 1
    $(".right").children().css("display", "none");
    $(section).css("display", "block");
    loadingDisplay(".rightLoad",true)
    if(section=="#makePublic"){
        checkFilePermissionInitial($(".right").data("fileInfo").path)
    }else if(section=="#metadata"){
        checkMetaDataInitial($(".right").data("fileInfo").path)
    }else{
    	analyseHistory()
    }
}

// Manage Link
function checkFilePermissionInitial(path){
    urlLink=location.origin+"/checkFilePermission?url="+path
    $.ajax({
        method: "GET",
        dataType: "html",
       	async: "true",
	url: urlLink,
	success: function (data, status, jqXHR) {
        	var userPerm=JSON.parse(data).paths[0]["user-permissions"]
                for(var i=0;i<=userPerm.length;i++){
        	        if(userPerm.length!=0 && userPerm[i].user=="anonymous" && userPerm[i].permission=="read"){
				manageLinkUI("create","https://data.cyverse.org/dav-anon"+path)
                        	break;
			}else{
                		manageLinkUI("remove",path)
			}
		}
		loadingDisplay(".rightLoad",false)
	},
	error: function (jqXHR, status, err) {
        	alert(err)
	},
        complete: function (jqXHR, status) {
        }
    });
}
function manageLinkUI(ele,path) {
    	if (ele == "remove") {
        	$('#remove').css("display", "none")
        	$('#create').css("display", "block")
        	$('#plink').val("")
        	$('#copy').css("visibility", 'hidden')
        	$('.fileStatus').removeClass("btn-warning")
        	$('.fileStatus').addClass("btn-info")
        	$('.fileStatus').html("Private")
    	} else {
        	$('#remove').css("display", "block")
        	$('#create').css("display", "none")
        	$('#plink').val(path)
        	$('#copy').css("visibility", 'visible')
        	$('.fileStatus').addClass("btn-warning")
        	$('.fileStatus').removeClass("btn-info")
        	$('.fileStatus').html("Public")
	}
}

function manageLinkOperations(ele){
	loadingDisplay(".rightLoad",true)
	var urlLink=""
	if (ele == "remove") {
		urlLink=location.origin+"/remPublicLink?url="+$(".right").data("fileInfo").path+"&is_anonymous=true"
	}else{
		urlLink=location.origin+"/setPublicLink?url="+$(".right").data("fileInfo").path+"&is_anonymous=true"
	}
	$.ajax({
	        method: "GET",
        	dataType: "html",
        	async: "true",
		url: urlLink,
		success: function (data, status, jqXHR) {
        		if(ele=="create"){
				if(JSON.parse(data).sharing[0].user=="anonymous" && JSON.parse(data).sharing[0].paths[0].permission=="read"){
					manageLinkUI(ele,JSON.parse(data).sharing[0].paths[0].path)
				}
			}else{
				if(JSON.parse(data).unshare[0].user=="anonymous" && JSON.parse(data).unshare[0].unshare[0].success==true){
					manageLinkUI(ele,JSON.parse(data).unshare[0].unshare[0].path)
				}
			}
        		loadingDisplay(".rightLoad",false)
		},
        	error: function (jqXHR, status, err) {
			alert(err)
		},
        	complete: function (jqXHR, status) {
		}
	});
}

// MetaData
function checkMetaDataInitial(path){
    loadingDisplay(".rightLoad",true)
//Get Species and Version List
    $(".species").children("button").html("Species...")
    $(".version").children("button").html("Version...")
    $('.fgcp').val("#0000FF")
    $('.bgcp').val("#FFFFFF")
    $.ajax({
        method: "GET",
        dataType: "html",
        async: "true",
	url: location.origin+"/getSpeciesVersionList/",
	success: function (data, status, jqXHR){
            specVer=JSON.parse(data)
        },
        error: function (jqXHR, status, err) {
            alert("Error: "+err)
	},
            complete: function (jqXHR, status) {
        }
   });
//check the Metadata data for the file
   retrieveMetaData($(".right").data("fileInfo").id)
}
function retrieveMetaData(fileId){
        var metaData=""
        $.ajax({
        method: "GET",
        dataType: "html",
        async: "true",
		url: location.origin+"/retrieveMetaData?fileId="+fileId,
	    success: function (data, status, jqXHR) {
		metaDataUI(JSON.parse(data))
            	loadingDisplay(".rightLoad",false)
        },
        error: function (jqXHR, status, err) {
            alert(err)
	    },
            complete: function (jqXHR, status) {
        }
	});
}
function metaDataUI(metaData){
    for(i=0;i<metaData["avus"].length;i++){
        if(metaData["avus"][i]["attr"]=="Genome"){
            var genome=metaData["avus"][i]["value"].split(";")
            $(".species").children("button").html(genome[0])
            $(".version").children("button").html(genome[1])
        }else if(metaData["avus"][i]["attr"]=="foreground"){
            $('.fgcp').val(metaData["avus"][i]["value"])
        }else if(metaData["avus"][i]["attr"]=="background"){
            $('.bgcp').val(metaData["avus"][i]["value"])
        }
    }
    $(".species").children("div").html("")
    var specLen=Object.keys(specVer).length;
    for(i=0;i<specLen;i++){
        $(".species").children("div").append("<a class='dropdown-item' onclick='dropDownPopulate("+i+")'>"+Object.keys(specVer)[i]+"</a>")
    }
}
function dropDownPopulate(n,data){
    $(".species").children("button").html(Object.keys(specVer)[n])
    $(".version").children("button").html("Version...")
    $(".version").children("div").html("")
    var verData=specVer[Object.keys(specVer)[n]]
    var verlen=verData.length;
    for(i=0;i<verlen;i++){
        $(".version").children("div").append("<a class='dropdown-item' onclick='$(\".version\").children(\"button\").html(\""+verData[i]+"\")'>"+verData[i]+"</a>")
    }
}
function saveMetaData(){
    loadingDisplay(".rightLoad",true)
    var fd = new FormData();
    var genome=$(".species").children("button").html()+";"+$(".version").children("button").html()
    fd.append( 'fileId', $(".right").data("fileInfo").id );
    fd.append('data', '[{"attr":"Genome","unit":"integratedGenomeBrowser","value":"'+genome+'"},{"attr":"foreground","unit":"integratedGenomeBrowser","value":"'+$('.fgcp').val()+'"},{"attr":"background","unit":"integratedGenomeBrowser","value":"'+$('.bgcp').val()+'"}]')

    $.ajax({
        url: location.origin + '/saveMetaData/',
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
            alert("Metadata Saved!");
            loadingDisplay(".rightLoad",false)
        },
        error: function (jqXHR, status, err) {
            alert(err)
	    },
            complete: function (jqXHR, status) {
        }
	});
}
function copyLink(){
	$("#plink").attr({disabled:false});
	$("#plink").select();
	document.execCommand("copy");
	$("#plink").attr({disabled:true});
}
function closerp() {
    $(".middle").removeClass("col-8")
    $(".middle").addClass("col-10")
    $(".right").css("display", "none")
    rpStatus = 0;
}


